package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;

public final class DocumentSkeleton implements Runnable {
    private final MessageManager messageManager;
    private final Document document;

    public DocumentSkeleton(Document document) {
        this.messageManager = new MessageManager();
        System.out.println("Connect using address:  "+ messageManager.getMyAddress());
        this.document = document;
    }

    public NetworkAddress getAddress(){
        return messageManager.getMyAddress();
    }

    private void sendEmptyReply(MethodCallMessage request){
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleGetText(MethodCallMessage request){
        String text = document.getText();
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("text", text);
        messageManager.send(reply, request.getOriginator());
    }


    private void handleSetText(MethodCallMessage request){
        String text = request.getParameter("text");
        document.setText(text);
        sendEmptyReply(request);
    }

    private void handleAppend(MethodCallMessage request){
        char c = request.getParameter("c").charAt(0);
        document.append(c);
        sendEmptyReply(request);
    }

    private void handleSetChar(MethodCallMessage request){
        int position = Integer.parseInt(request.getParameter("position"));
        char c = request.getParameter("c").charAt(0);
        document.setChar(position, c);
        sendEmptyReply(request);
    }

    private void handleRequests(MethodCallMessage request){
        String methodName = request.getMethodName();

        switch (methodName) {
            case "getText":
                handleGetText(request);
                break;
            case "setText":
                handleSetText(request);
                break;
            case "append":
                handleAppend(request);
                break;
            case "setChar":
                handleSetChar(request);
                break;
            default:
                sendEmptyReply(request);
                break;
        }
    }

    public void run(){
        while(true){
            MethodCallMessage request = messageManager.wReceive();
            handleRequests(request);
        }
    }
}
