package be.kdg.examen.gedistribueerde.client;

import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;
import be.kdg.examen.gedistribueerde.server.ServerStub;

public class StartNonDistributed {
    public static void main(String[] args) {
        //parameters van args geven het serveraddress, waarmee we een serverstub maken! Deze repliceert de impl maar
        // ipv van deze uit te voeren stuurt het een message naar de server. Die message wordt opgevangen door de
        // skeleton die het dan weer naar de serverImpl stuurt.
        int port = Integer.parseInt(args[1]);
        NetworkAddress serverAddress = new NetworkAddress(args[0], port);

        DocumentImpl document = new DocumentImpl();
        DocumentSkeleton documentSkeleton = new DocumentSkeleton(document);
        Thread thread = new Thread(documentSkeleton);
        thread.start();


        Server server = new ServerStub(serverAddress, documentSkeleton.getAddress());

        Client client = new Client(server, document);
        client.run();
    }
}
