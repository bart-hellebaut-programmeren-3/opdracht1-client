package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communication.NetworkAddress;
import be.kdg.examen.gedistribueerde.server.Server;

public final class ServerStub implements Server {
    private final MessageManager messageManager;
    private final NetworkAddress serverAddress;
    private final NetworkAddress documentSkeletonAddress;

    public ServerStub(NetworkAddress serverAddress, NetworkAddress documentSkeletonAddress) {
        this.messageManager = new MessageManager();
        this.documentSkeletonAddress = documentSkeletonAddress;
        this.serverAddress = serverAddress;
    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }

    @Override
    public void log(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "log");
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);
        checkEmptyReply();
    }

    @Override
    public Document create(String s) {
        //send message
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "create");
        message.setParameter("text", s);
        messageManager.send(message, serverAddress);

        //get reply
        MethodCallMessage reply = messageManager.wReceive();
        String text = reply.getParameter("document.text");
        return new DocumentImpl(text);
    }

    @Override
    public void toUpper(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toUpper");
        message.setParameter("ip", documentSkeletonAddress.getIpAddress());
        message.setParameter("port", String.valueOf(documentSkeletonAddress.getPortNumber()));
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        //get reply
        checkEmptyReply();
    }

    @Override
    public void toLower(Document document) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "toLower");
        message.setParameter("ip", documentSkeletonAddress.getIpAddress());
        message.setParameter("port", String.valueOf(documentSkeletonAddress.getPortNumber()));
        message.setParameter("text", document.getText());
        messageManager.send(message, serverAddress);

        //get reply
        checkEmptyReply();
    }

    @Override
    public void type(Document document, String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "type");
        message.setParameter("ip", documentSkeletonAddress.getIpAddress());
        message.setParameter("port", String.valueOf(documentSkeletonAddress.getPortNumber()));
        message.setParameter("text", text);
        messageManager.send(message, serverAddress);

        checkEmptyReply();

    }
}
